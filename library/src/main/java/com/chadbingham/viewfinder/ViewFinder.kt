@file:Suppress("UNCHECKED_CAST", "unused")

package com.chadbingham.viewfinder

import android.app.Activity
import android.app.Dialog
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.DialogInterface
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.util.Log
import android.view.View
import com.chadbingham.viewfinder.ViewFinder.Registry.register
import com.chadbingham.viewfinder.ViewFinder.Registry.release
import com.chadbingham.viewfinder.ViewFinder.dataObserver
import com.chadbingham.viewfinder.ViewFinder.look
import com.chadbingham.viewfinder.ViewFinder.viewFinder
import java.util.*
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun <V : View> View.findView(id: Int): ReadOnlyProperty<View, V> = ViewFinder.find(id, viewFinder)
fun <V : View> Activity.findView(id: Int): ReadOnlyProperty<Activity, V> = ViewFinder.find(id, viewFinder)
fun <V : View> Dialog.findView(id: Int): ReadOnlyProperty<Dialog, V> = ViewFinder.find(id, viewFinder)
fun <V : View> DialogFragment.findView(id: Int): ReadOnlyProperty<DialogFragment, V> = ViewFinder.find(id, viewFinder)
fun <V : View> Fragment.findView(id: Int): ReadOnlyProperty<Fragment, V> = ViewFinder.find(id, viewFinder)
fun <V : View> ViewHolder.findView(id: Int): ReadOnlyProperty<ViewHolder, V> = ViewFinder.find(id, viewFinder)

fun <V : View> View.findViewOptional(id: Int): ReadOnlyProperty<View, V?> = look(id, viewFinder)
fun <V : View> Activity.findViewOptional(id: Int): ReadOnlyProperty<Activity, V?> = look(id, viewFinder)
fun <V : View> Dialog.findViewOptional(id: Int): ReadOnlyProperty<Dialog, V?> = look(id, viewFinder)
fun <V : View> DialogFragment.findViewOptional(id: Int): ReadOnlyProperty<DialogFragment, V?> = look(id, viewFinder)
fun <V : View> Fragment.findViewOptional(id: Int): ReadOnlyProperty<Fragment, V?> = look(id, viewFinder)
fun <V : View> ViewHolder.findViewOptional(id: Int): ReadOnlyProperty<ViewHolder, V?> = look(id, viewFinder)

fun View.showWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, true)
fun Activity.showWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, true)
fun Dialog.showWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, true)
fun DialogFragment.showWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, true)
fun Fragment.showWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, true)
fun ViewHolder.showWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, true)

fun View.hideWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, false)
fun Activity.hideWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, false)
fun Dialog.hideWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, false)
fun DialogFragment.hideWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, false)
fun Fragment.hideWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, false)
fun ViewHolder.hideWhenEmpty(view: View, adapter: Adapter<*>) = dataObserver(view, adapter, false)

fun View.dropViews() = release(this)
fun Activity.dropViews() = release(this)
fun Dialog.dropViews() = release(this)
fun DialogFragment.dropViews() = release(this)
fun Fragment.dropViews() = release(this)
fun ViewHolder.dropViews() = release(this)

object ViewFinder {

    @Suppress("MemberVisibilityCanPrivate")
    var logging = false

    private const val TAG = "ViewFinder"

    internal inline fun <reified T : Any> T.observer(view: View, adapter: Adapter<*>, show: Boolean) {
        register(this, DataObserver(this, view, adapter, show))
    }

    internal inline fun <reified T : Any> T.dataObserver(view: View, adapter: Adapter<*>, show: Boolean) {
        register(this, DataObserver(this, view, adapter, show))
    }

    internal val View.viewFinder: View.(Int) -> View?
        get() = { findViewById(it) }

    internal val Activity.viewFinder: Activity.(Int) -> View?
        get() = { findViewById(it) }

    internal val Dialog.viewFinder: Dialog.(Int) -> View?
        get() = { findViewById(it) }

    internal val DialogFragment.viewFinder: DialogFragment.(Int) -> View?
        get() = { dialog?.findViewById(it) ?: view?.findViewById(it) }

    internal val Fragment.viewFinder: Fragment.(Int) -> View?
        get() = { view?.findViewById(it) }

    internal val ViewHolder.viewFinder: ViewHolder.(Int) -> View?
        get() = { itemView.findViewById(it) }

    internal fun <T : Any, V : View> find(id: Int, finder: T.(Int) -> View?): ReadOnlyProperty<T, V> {
        return ViewDelegate(id) { t: T, property -> t.finder(id) as V? ?: viewNotFound(id, property) }
    }

    internal fun <T : Any, V : View> look(id: Int, finder: T.(Int) -> View?): ReadOnlyProperty<T, V?> {
        return ViewDelegate(id) { t: T, _ -> t.finder(id) as V? }
    }

    internal interface ViewContract<T, V : View?> {
        var target: T
        var view: V
        val id: Int
        fun release()
    }

    internal object Registry {
        private val targets = WeakHashMap<Any, TargetViewContracts>()

        internal fun register(target: Any, reference: ViewContract<*, *>) {
            if (targets[target] != null) {
                log("Contract found for target: $target")
            }

            val binding = targets[target] ?: TargetViewContracts(target)
            binding.register(reference)
            targets.put(target, binding)
        }

        internal fun release(target: Any) {
            val binding = targets[target]
            if (binding != null) {
                binding.releaseContracts()
                targets.remove(binding)
            } else {
                w("No ViewContract to release for $target")
            }
        }
    }

    internal data class TargetViewContracts(private val target: Any)
        : LifecycleObserver, View.OnAttachStateChangeListener, DialogInterface.OnDismissListener {

        private val contracts = WeakHashMap<Int, ViewContract<*, *>>()
        private val observers = WeakHashMap<Int, ViewContract<*, *>>()

        init {
            (target as? LifecycleOwner)?.lifecycle?.addObserver(this)

            (target as? View)?.addOnAttachStateChangeListener(this)

            (target as? DialogFragment)?.dialog?.setOnDismissListener(this)
        }

        /**
         * Used when the target is a DialogFragment
         */
        override fun onDismiss(dialog: DialogInterface?) {
            log("Target Dialog dismissed - releasing contracts")
            releaseContracts()
        }

        /**
         * Used when the target is a View
         */
        override fun onViewDetachedFromWindow(view: View?) {
            log("Target View detached - releasing contracts")
            releaseContracts()
        }

        override fun onViewAttachedToWindow(view: View?) {}

        /**
         * Used when the target is a Lifecycle Owner
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun drop() {
            log("Target View destroyed - releasing contracts")
            releaseContracts()
        }

        internal fun register(contract: ViewContract<*, *>) {
            if (contract is ViewDelegate<*, *>) {
                contracts[contract.id] = contract
                log("Registered contract #${contracts.size}: $contract")
            } else if (contract is DataObserver) {
                observers[contract.id] = contract
                log("Registered dataObserver #${observers.size}: $contract")
            }
        }

        internal fun releaseContracts() {
            if (contracts.isEmpty() && observers.isEmpty()) {
                log("No contracts or observers to release")
            }

            if (contracts.isNotEmpty()) {
                log("Releasing ${contracts.size} contracts")
                contracts.values.forEach { it.release() }
                contracts.clear()
            }

            if (observers.isNotEmpty()) {
                log("Releasing ${observers.size} observers")
                observers.values.forEach { it.release() }
                observers.clear()
            }

            (target as? LifecycleOwner)?.lifecycle?.removeObserver(this)

            (target as? View)?.removeOnAttachStateChangeListener(this)

            (target as? DialogFragment)?.dialog?.setOnDismissListener(null)
        }
    }

    private data class ViewDelegate<T : Any, V : View?>(
            override val id: Int,
            override var view: V? = null,
            override var target: T? = null,
            private val initializer: (T, KProperty<*>) -> V)
        : ReadOnlyProperty<T, V>, ViewContract<T?, V?> {

        private object EMPTY

        private var value: Any? = EMPTY
        private var name: String? = null //only used for logging

        override fun getValue(thisRef: T, property: KProperty<*>): V {
            name = property.name
            target = thisRef
            register(thisRef, this)

            if (value == EMPTY) {
                value = initializer(thisRef, property)
                log("Value \"$name\" initialized")
            }

            return value as V
        }

        override fun release() {
            if (name != null && value != null)
                log("releasing '$name'(${value!!::class.java.simpleName})")
            value = EMPTY
        }

        override fun equals(other: Any?): Boolean {
            return other != null
                    && other is ViewDelegate<*, *>
                    && other.target === target
                    && other.id == id
                    && other.name == name
        }

        override fun hashCode(): Int {
            var result = 17
            result = 31 * result + (target?.hashCode() ?: 0)
            result = 31 * result + id.hashCode()
            return result
        }
    }

    internal class DataObserver(
            override var target: Any,
            override var view: View,
            private val adapter: Adapter<*>,
            private val show: Boolean = true)
        : RecyclerView.AdapterDataObserver(), ViewContract<Any, View> {

        override val id = view.id

        init {
            adapter.registerAdapterDataObserver(this)
            showView()
        }

        override fun release() {
            log("Unregistering data dataObserver")
            adapter.unregisterAdapterDataObserver(this)
        }

        private fun showView() {
            view.visibility = if (adapter.itemCount == 0) {
                if (show)
                    View.VISIBLE
                else
                    View.GONE
            } else {
                if (show)
                    View.GONE
                else
                    View.VISIBLE
            }
        }

        override fun onChanged() = showView()
        override fun onItemRangeRemoved(posStart: Int, itemCount: Int) = showView()
        override fun onItemRangeMoved(fromPos: Int, toPos: Int, itemCount: Int) = showView()
        override fun onItemRangeInserted(posStart: Int, itemCount: Int) = showView()
        override fun onItemRangeChanged(posStart: Int, itemCount: Int) = showView()
        override fun onItemRangeChanged(posStart: Int, itemCount: Int, payload: Any?) = showView()
    }

    private fun viewNotFound(id: Int, property: KProperty<*>): Nothing {
        throw IllegalStateException("View ID $id for '${property.name}' not found.")
    }

    private fun log(message: String) {
        if (ViewFinder.logging)
            Log.i(TAG, message)
    }

    private fun w(message: String) = Log.w(TAG, message)
}