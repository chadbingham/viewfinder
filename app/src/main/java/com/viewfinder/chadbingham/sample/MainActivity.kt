package com.viewfinder.chadbingham.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.chadbingham.viewfinder.findView

class MainActivity : AppCompatActivity() {

    private val text: TextView by findView(R.id.text)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
